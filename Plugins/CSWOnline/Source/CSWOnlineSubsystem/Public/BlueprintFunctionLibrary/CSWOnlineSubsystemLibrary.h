/**
* Copyright (c) 2018 Cronofear Softworks, Inc. All Rights Reserved.
*
* Developed by Kevin Yabar Garces
*/

#pragma once

#include "Core/Public/CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "CSWOnlineSubsystemLibrary.generated.h"


class UTexture2D;

USTRUCT(BlueprintType)
struct FCSWDirectoryData
{
	GENERATED_USTRUCT_BODY()

	/** The time that the file or directory was originally created, or FDateTime::MinValue if the creation time is unknown */
	UPROPERTY(BlueprintReadOnly, Category = "CSW|DirectoryData")
		FDateTime CreationTime;

	/** The time that the file or directory was last accessed, or FDateTime::MinValue if the access time is unknown */
	UPROPERTY(BlueprintReadOnly, Category = "CSW|DirectoryData")
		FDateTime AccessTime;

	/** The time the the file or directory was last modified, or FDateTime::MinValue if the modification time is unknown */
	UPROPERTY(BlueprintReadOnly, Category = "CSW|DirectoryData")
		FDateTime ModificationTime;

	/** True if this data is for a directory, false if it's for a file */
	UPROPERTY(BlueprintReadOnly, Category = "CSW|DirectoryData")
		bool bIsDirectory = true;

	/** True if this file is read-only */
	UPROPERTY(BlueprintReadOnly, Category = "CSW|DirectoryData")
		bool bIsReadOnly = true;

	/** True if file or directory was found, false otherwise. Note that this value being true does not ensure that the other members are filled in with meaningful data, as not all file systems have access to all of this data */
	UPROPERTY(BlueprintReadOnly, Category = "CSW|DirectoryData")
		bool bIsValid = true;
};

/**
 * Static libraries exposed to Blueprints.
 * These libraries can handle directories and files in a project.
 */
UCLASS()
class CSWONLINESUBSYSTEM_API UCSWOnlineSubsystemLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	/**
	* Get the path of "/Project/Saved/Screenshots/"
	*/
	UFUNCTION(BlueprintPure, Category = "CSW|OnlineSubsystem::Utils", meta = (DisplayName = "CSW::Get Path of /Screenshots"))
		static FString GetPathScreenshots();
	/**
	* Get the name of the Platform
	*/
	UFUNCTION(BlueprintPure, Category = "CSW|OnlineSubsystem::Utils")
		static FString GetPlatformName();
	/**
	* Get the absolute path of the ".../Project/" folder.
	*/
	UFUNCTION(BlueprintPure, Category = "CSW|OnlineSubsystem::Utils", meta = (DisplayName = "CSW::Get Path of /Project"))
		static FString GetPathProject();
	/**
	* Get the absolute path of the ".../Project/Saved/" folder.
	*/
	UFUNCTION(BlueprintPure, Category = "CSW|OnlineSubsystem::Utils", meta = (DisplayName = "CSW::Get Path of /Saved"))
		static FString GetPathSaved();
	/**
	* Get the absolute path of the ".../Project/Saved/SaveGames/" folder.
	*/
	UFUNCTION(BlueprintPure, Category = "CSW|OnlineSubsystem::Utils", meta = (DisplayName = "CSW::Get Path of /SaveGames"))
		static FString GetPathSaveGames();

	/**
	* Verify if a directory exists.
	* @param Path				Path to check.
	* @return					True if the directory exists.
	*/
	UFUNCTION(BlueprintPure, Category = "CSW|OnlineSubsystem::Utils", meta = (DisplayName = "CSW::Does Directory Exist"))
		static bool DoesDirectoryExists(const FString& Path);
	/**
	* Verify if a file in a directory exists.
	* @param Path				Path to check.
	* @return					True if the directory exists.
	*/
	UFUNCTION(BlueprintPure, Category = "CSW|OnlineSubsystem::Utils", meta = (DisplayName = "CSW::Does File Exist"))
		static bool DoesFileExists(const FString& Path, const FString& FileName, const FString& Extension);

	/**
	* Get metadata of file or directory.
	*  @param Path					Path of the directory. This is the only one required.
	*  @param FileName				Name of the file, in case the metadata of the file needs to be obtained.
	*  @param FileExtension			The extension of the file.
	*/
	UFUNCTION(BlueprintPure, Category = "CSW|OnlineSubsystem::Utils", meta = (DisplayName = "CSW::Get Metadata from Dir or File"))
		static void GetMetaDataFrom(const FString& Path, const FString& FileName, const FString& Extension, FCSWDirectoryData& Metadata);

	/**
	* Attempt to create a directory tree.
	* Doesn't do anything if the directory already exists, in which case it returns false.
	* Ex1: GetPathSaved()+"Test/" will attempt to create a "Test/" directory inside ".../Project/Saved/".
	* Ex2: GetPathProject()+"Test/a/b/" will attempt to create a "Test/a/b/" directory TREE inside ".../Project/".
	* @param Path				Path that should also contain non-existent directories.
	* @return					True if the directory is successfully created.
	*/
	UFUNCTION(BlueprintCallable, Category = "CSW|OnlineSubsystem::Utils", meta = (DisplayName = "CSW::Create Directory"))
		static bool MakeDirectory(const FString& Path);

	/**
	*  Get a list of file names that exist inside a directory. Filtered by FileExtension.
	*  @param Path					Path to search the files (ex. using GetPath...()).
	*  @param FileNames				List of names of the save files found.
	*  @param FileExtension			Extension to search. Can be "*" for search all or a custom one: ex. ".png".
	*  @param bRemoveExtensions		Remove extension to FileNames.
	*/
	UFUNCTION(BlueprintPure, Category = "CSW|OnlineSubsystem::Utils", meta = (DisplayName = "CSW::Get Files In Directory", AdvancedDisplay = "bRemoveExtensions"))
		static void GetFilesInDirectory(TArray<FString>& FileNames, const FString& Path, const FString& FileExtension = "*", bool bRemoveExtensions = true);

	/**
	* Delete a file given the parameters.
	* Ex: Path: GetPathSaved(), FileName: "new file", FileExtension: ".txt".
	*  @param Path					Path to search the file (ex. using GetPath...()).
	*  @param FileName				Name of the file to delete.
	*  @param FileExtension			Extension of the file to delete.
	*/
	UFUNCTION(BlueprintCallable, Category = "CSW|OnlineSubsystem::Utils", meta = (DisplayName = "CSW::Delete File"))
		static bool DeleteFile(const FString& Path, const FString& FileName, const FString& FileExtension);
	/**
	* Move a File from a SourcePath into a DestinationPath.
	*  @param SourcePath			Source Path to look the file.
	*  @param FileName				Name of the file to Move.
	*  @param FileExtension			Extension of the file to Move.
	*  @param DestinationPath		Destination Path where the file will be moved.
	*/
	UFUNCTION(BlueprintCallable, Category = "CSW|OnlineSubsystem::Utils", meta = (DisplayName = "CSW::Move File"))
		static bool MoveFile(const FString& SourcePath, const FString& FileName, const FString& FileExtension, const FString& DestinationPath);
	/**
	* Copy a File from a SourcePath into a DestinationPath.
	*  @param SourcePath			Source Path to look the file.
	*  @param FileName				Name of the file to Copy.
	*  @param FileExtension			Extension of the file to Copy.
	*  @param DestinationPath		Destination Path where the file will be Copied.
	*/
	UFUNCTION(BlueprintCallable, Category = "CSW|OnlineSubsystem::Utils", meta = (DisplayName = "CSW::Copy File"))
		static bool CopyFile(const FString& SourcePath, const FString& FileName, const FString& FileExtension, const FString& DestinationPath);
};